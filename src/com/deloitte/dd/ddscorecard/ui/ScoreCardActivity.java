package com.deloitte.dd.ddscorecard.ui;

import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.os.Bundle;
import android.widget.Toast;

import com.deloitte.dd.ddscorecard.R;
import com.deloitte.dd.ddscorecard.ui.task.LoaderResult;
import com.deloitte.dd.ddscorecard.ui.task.SendRatingLoader;

public class ScoreCardActivity extends Activity implements LoaderCallbacks<LoaderResult<ResponseDto>> {

    public static final String SCORE = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scorecard_activity);

        Bundle bundle = new Bundle();
        bundle.putString(SCORE, "5");
        getLoaderManager().restartLoader(0, bundle, this);

    }

    @Override
    public Loader<LoaderResult<ResponseDto>> onCreateLoader(int id, Bundle args) {
        return new SendRatingLoader(this, args);
    }

    @Override
    public void onLoadFinished(Loader<LoaderResult<ResponseDto>> arg0, LoaderResult<ResponseDto> arg1) {

        if (arg1.getData() != null) {

            if (arg1.getData().result.equals("true")) {

                Toast.makeText(this, "Done !", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(this, "Failed !", Toast.LENGTH_SHORT).show();

            }
        } else {

            Toast.makeText(this, "Failed !", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onLoaderReset(Loader<LoaderResult<ResponseDto>> arg0) {
        // TODO Auto-generated method stub

    }
}
