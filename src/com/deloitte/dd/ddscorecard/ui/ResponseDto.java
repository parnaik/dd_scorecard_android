package com.deloitte.dd.ddscorecard.ui;

import com.google.gson.annotations.SerializedName;

public class ResponseDto {

    @SerializedName("result")
    public String result;

}
