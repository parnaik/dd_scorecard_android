package com.deloitte.dd.ddscorecard.ui.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

public class ScoreCardCustomView extends View {

    private int mColorCircleTwo = Color.parseColor("#808080");
    private int colorShadow = Color.parseColor("#66A0A0A0");

    static int CIRCLE_LAYER_WIDTH = 64;
    private static int STROKE_WIDTH = 4;
    private static int STROKE_WIDTH_GUIDELINE = 2;
    private static int ANIMATION_DURATION = 333;

    private Paint mPaint;
    private DecelerateInterpolator mDecInterpolator;
    private OvershootInterpolator mOvershootInterpolator;
    private BounceInterpolator mBounceInterpolator;
    private ObjectAnimator mObjectAnimator;
    private int mBaseRadius;
    private int mCurrentRadius;
    private int mCircleAnimDirection = 1;
    private Canvas mCanvas;

    private Point centerPoint = new Point();
    private Point currentPoint = new Point();

    private boolean isCircleReleased;
    private CircleControl circleControl;
    private int mInterpolatorStartTime;

    public ScoreCardCustomView(Context context) {
        this(context, null);
    }

    public ScoreCardCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();

        circleControl = new CircleControl();
        mDecInterpolator = new DecelerateInterpolator();
        mOvershootInterpolator = new OvershootInterpolator();
        mBounceInterpolator = new BounceInterpolator();
    }

    private void initPaint() {

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Cap.BUTT);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        mCurrentRadius = mBaseRadius = width / 12;
        centerPoint.set(width / 2, height / 10 * 6);
        setMeasuredDimension(width, height);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

        case MotionEvent.ACTION_DOWN:
            isCircleReleased = false;
            break;

        case MotionEvent.ACTION_MOVE:
            if ((event.getY() < (getHeight() - mBaseRadius)) && (event.getY() >= centerPoint.y)) {
                currentPoint.set((int) event.getX(), (int) event.getY());
                validateTouchAreaAndDraw();
            }
            break;

        case MotionEvent.ACTION_UP:
            isCircleReleased = true;
            break;

        default:
            break;

        }

        invalidate();
        return true;
    }

    private void validateTouchAreaAndDraw() {

        if (isNextLevel() || isPreviousLevel()) {
            mCurrentRadius = circleControl.getCurrentLevelRadius();
            mInterpolatorStartTime = (int) AnimationUtils.currentAnimationTimeMillis();
        }

    }

    private boolean isNextLevel() {

        if (getDiffBetweenPoints(currentPoint, centerPoint) >= circleControl.getNextLevelRadius()) {
            if (circleControl.currentLevel < 4) {
                circleControl.setNextLevel();
                return true;
            }
        }

        return false;
    }

    private boolean isPreviousLevel() {

        if (getDiffBetweenPoints(currentPoint, centerPoint) <= circleControl.getCurrentLevelRadius()) {
            if (circleControl.currentLevel > 0) {
                circleControl.setPreviousLevel();
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mCanvas = canvas;
        mCanvas.save();

        drawbaseCirlce();
        drawGuidelineCircles();

        drawNextLevel();

        // showScore();

        showScoreText();

        mCanvas.restore();
    }

    private void drawbaseCirlce() {

        mPaint.setStyle(Paint.Style.FILL);

        mPaint.setColor(colorShadow);
        mCanvas.drawCircle(centerPoint.x, centerPoint.y, mBaseRadius + 10, mPaint);

    }

    private void drawNextLevel() {

        mPaint.setColor(circleControl.getCurrentStateColor());

        int controllerY = (centerPoint.y > currentPoint.y) ? centerPoint.y : currentPoint.y;

        if(isCircleReleased){
            mCanvas.drawCircle(centerPoint.x, centerPoint.y + circleControl.getCurrentLevelRadius(), mBaseRadius, mPaint);
        }else {
            mCanvas.drawCircle(centerPoint.x, controllerY, mBaseRadius, mPaint);
        }
        invalidate();
    }

    private void drawGuidelineCircles() {
        mPaint.setColor(colorShadow);
        int yOffset = getHeight() / 6;
        int width = getWidth();
        float interpolatorMultiplier = 1f;

        int currentAnimationTime = (int) AnimationUtils.currentAnimationTimeMillis();

        for (int i = 0; i < 5; i++) {

            if (i <= circleControl.currentLevel) {
                if ((currentAnimationTime < mInterpolatorStartTime + ANIMATION_DURATION)
                        && (i == circleControl.currentLevel)) {
                    interpolatorMultiplier = mBounceInterpolator
                            .getInterpolation((currentAnimationTime - mInterpolatorStartTime)
                                    / (float) ANIMATION_DURATION);
                }
                mPaint.setColor(circleControl.getCurrentStateColor());
                mCanvas.drawCircle(width / 12 * 2 * (i + 1), centerPoint.y - yOffset, mBaseRadius / 3
                        * interpolatorMultiplier, mPaint);
            } else {
                mPaint.setColor(colorShadow);
                mCanvas.drawCircle(width / 12 * 2 * (i + 1), centerPoint.y - yOffset, mBaseRadius / 10, mPaint);
            }
        }
    }

    private void showScore() {
        mPaint.setStyle(Paint.Style.FILL);
        mCanvas.drawText(String.valueOf(circleControl.currentLevel + 1), centerPoint.x - 30f, 200f, mPaint);
    }

    private void showScoreText() {
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(120f);
        mCanvas.drawText(String.valueOf(circleControl.currentLevel + 1), centerPoint.x - 30f, 200f, mPaint);
    }

    private static float getDiffBetweenPoints(Point A, Point B) {
        int diffY = A.y - B.y;
        return diffY;
    }

    private class CircleControl {
        int currentLevel = 0;

        private int[] colors = new int[] { Color.parseColor("#1FCA88"), Color.parseColor("#20c9a1"),
                Color.parseColor("#20c9be"), Color.parseColor("#1fb6c6"), Color.parseColor("#209cc9") };

        public CircleControl() {
            // TODO Auto-generated constructor stub
        }

        public int getCurrentStateColor() {
            return colors[currentLevel];
        }

        public void setNextLevel() {
            if (currentLevel < 4) {
                currentLevel++;
            }
        }

        public void setPreviousLevel() {
            if (currentLevel > 0) {
                currentLevel--;
            }
        }

        public int getCurrentLevelRadius() {
            return (CIRCLE_LAYER_WIDTH * (currentLevel));
        }

        public int getNextLevelRadius() {
            return (CIRCLE_LAYER_WIDTH * (currentLevel + 1));
        }

    }

}
