package com.deloitte.dd.ddscorecard.ui.task;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;

public class RatingClient<T> {

    private Gson mGson;
    private Class<T> mClazz;
    private String mUrl;

    public RatingClient(String url, Class<T> clazz) {
        mUrl = url;
        mClazz = clazz;
    }

    public T performGsonRequest() throws Exception {

        mGson = new Gson();
        return mGson.fromJson(getHttpResponseData(mUrl), mClazz);

    }

    protected String getHttpResponseData(String requestUrl) throws Exception {

        System.out.println(requestUrl);
        String data = null;
        URL url = new URL(requestUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        data = readStream(con.getInputStream());

        System.out.println(data);
        return data;
    }

    protected String readStream(InputStream is) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                outputStream.write(i);
                i = is.read();
            }
            return outputStream.toString();
        } catch (IOException e) {
            return "";
        }
    }
}