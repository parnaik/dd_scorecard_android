package com.deloitte.dd.ddscorecard.ui.task;

import android.content.Context;
import android.os.Bundle;

import com.deloitte.dd.ddscorecard.ui.ResponseDto;
import com.deloitte.dd.ddscorecard.ui.ScoreCardActivity;

public class SendRatingLoader extends BaseTaskLoader<ResponseDto> {

    public String URL = "https://www.google.co.in/";
    public String mRating;

    public SendRatingLoader(Context context, Bundle bundle) {
        super(context);
        mRating = bundle.getString(ScoreCardActivity.SCORE);
    }

    @Override
    protected ResponseDto loadDataInBackground() throws Exception {

        String url = URL;
        RatingClient<ResponseDto> client = new RatingClient<ResponseDto>(url, ResponseDto.class);
        return client.performGsonRequest();
    }
}
