package com.deloitte.dd.ddscorecard.ui.task;

import android.content.AsyncTaskLoader;
import android.content.Context;

public abstract class BaseTaskLoader<T> extends AsyncTaskLoader<LoaderResult<T>> {

    private LoaderResult<T> mResult;

    public BaseTaskLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if (mResult != null) {
            deliverResult(mResult);
        }
        if (takeContentChanged() || mResult == null) {
            forceLoad();
        }
    }

    @Override
    public void deliverResult(LoaderResult<T> result) {
        if (isReset()) {
            // An async query came in while the loader is stopped
            return;
        }
        mResult = result;

        if (isStarted()) {
            super.deliverResult(result);
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();

        mResult = null;
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public final LoaderResult<T> loadInBackground() {

        LoaderResult<T> result = new LoaderResult<T>();
        T data = null;
        try {
            data = loadDataInBackground();
        } catch (Exception e) {
            result.setException(e);
        }

        result.setData(data);

        return result;
    }

    protected abstract T loadDataInBackground() throws Exception;
}
